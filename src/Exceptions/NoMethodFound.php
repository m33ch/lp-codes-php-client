<?php

namespace Advision\Lp\Codes\Exceptions;

class NoMethodFound extends \Exception
{
    public function __construct($property, $class)
    {
        $message = 'Method %s not found on % class';

        parent::__construct(sprintf($message, $property, $class));
    }
}