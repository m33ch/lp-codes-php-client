<?php

use Advision\Lp\Codes\Authenticator;
use Advision\Lp\Codes\Client;
use Advision\Lp\Codes\Models\Campaign;

class CampaignTest {

    public static function create($client, $groupId = '5976159298c93807044f2eab')
    {
        $campaign = new \Advision\Lp\Codes\Models\Campaign();
        $userId = rand(1,99);
        $campaign->setForeignCampaignId(rand(1,99))
                 ->setForeignUserId($userId)
                 ->setType('LpCoupon')
                 ->setRules(['EXS', 'UNQ'])
                 ->setCodes([
                    ['contacts' => [], 'code' => rand(1,9999)],
                    ['contacts' => [], 'code' => rand(1,9999)],
                    ['contacts' => [], 'code' => rand(1,9999)],
                    ['contacts' => [], 'code' => rand(1,9999)],
                    ['contacts' => [], 'code' => rand(1,9999)],
                    ['contacts' => [], 'code' => rand(1,9999)],
                    ['contacts' => [], 'code' => rand(1,9999)]
                 ])
                 ->with("cid_{$console_id}_usr_{$userId}");

        return $client->save($campaign);
    }

    public static function update($client, $id, $data)
    {
        $group = Advision\Lp\Codes\Factories\Campaign::fromArray($data);

        $group->setId($id);

        return $client->save($group);
    }

    public static function find($client, $id)
    {
        $group = Advision\Lp\Codes\Factories\Campaign::fromArray([]);

        $group->setId($id);

        return $client->find($group);
    }

    public static function delete($client, $id)
    {
        $group = Advision\Lp\Codes\Factories\Campaign::fromArray([]);

        $group->setId($id);

        return $client->delete($group);
    }

}

// CREATE FORM
try {

    $auth = new Authenticator($console_id, $api_key, $api_password, $host);

    $token = $auth->getToken();

    $client = new Client($token);

    $campaign = CampaignTest::create($client);

    dd($campaign);

} catch (Exception $e) {
    dd($e->getMessage(), $e->getCode());
}

// UPDATE FORM
/*try {

    $auth = new Authenticator(1223, $api_key, $api_password);

    $token = $auth->getToken();

    $client = new Client($token);

    $form = FormApiTest::update($client, '5976f7c9d1f548389932929f', ['name' => 'Form modificato']);

    dd($form);

} catch (Exception $e) {
    dd($e->getMessage(), $e->getCode());
}*/
//FIND ONE FORM
/*try {

    $auth = new Authenticator(1223, $api_key, $api_password);

    $token = $auth->getToken();

    $client = new Client($token);

    $form = FormApiTest::find($client, '5976f7c9d1f548389932929f');

    dd($form);

} catch (Exception $e) {
    dd($e->getMessage(), $e->getCode());
}*/

//DELETE ONE FORM
/*try {

    $auth = new Authenticator(1223, $api_key, $api_password);

    $token = $auth->getToken();

    $client = new Client($token);

    $form = FormApiTest::delete($client, '5976f7c9d1f548389932929f');

    dd($form);

} catch (Exception $e) {
    dd($e->getMessage(), $e->getCode());
}*/
// $r = createForm($host, [
//     'auth_token' => $token,
//     'name' => 'form_' . rand(1, 5),
//     'formgroup_id' => "594785339248c528356af666"
// ]);
// dd($r);

// $r = updateForm($host, "5936b8c02047b406fe5a151b", [
//     'auth_token' => $token,
//     'name' => 'form_mod'
// ]);
// dd($r);

// $r = getForm($host, $token, "5936b8c02047b406fe5a151b");
// dd($r);

// $r = deleteForm($host, $token, "5936b8762047b406fe5a151a");
// dd($r);

// $r = listForms($host, $token, "593692a0c39de75980ff0c09");
// dd($r);

//$themes = getFormThemes($host, $token, '*');
//dd($themes);

// $f = setFormTheme($host, $token, "5936b8c02047b406fe5a151b", $themes->embedded[0]);
// dd($f);

// $list = getLists($host, $token);
// $f = setFormList($host, $token, "5936b8c02047b406fe5a151b", $list[0]->_id);
// dd($f);

// $config = getConfig($host, $token);
// $f = setFormFields($host, $token, "5936b8c02047b406fe5a151b", 'test', json_decode(json_encode($config->fields), true));
// dd($f);
