<?php

namespace Advision\Lp\Codes\Models;

use Advision\Lp\Codes\Contracts\Arrayable;
use Advision\Lp\Codes\Contracts\Jsonable;
use Advision\Lp\Codes\Exceptions\NoPropertyFound;

abstract class Model implements Arrayable, Jsonable
{
    protected $id = null;

    protected $integrationId = null;

    protected $integrationUserId = null;

    protected $relateds = [];

    public abstract function getEndpoint();

    protected abstract function factoryClass();

    public function getFactory()
    {

        $class = $this->factoryClass();

        if (class_exists($class))
        {
            return $class::getInstance();
        }
    }

    public function with($rel)
    {
        if (in_array($rel, $this->relateds)) return $this;

        $this->relateds[] = $rel;

        return $this;
    }

    public function emptyRelateds()
    {
        return empty($this->relateds);
    }

    public function getRelateds()
    {
        return implode('/',$this->relateds);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function __call($name, $arguments)
    {
        if (method_exists($this, $name) == false)
        {
            throw new NoPropertyFound($name, get_class($this));
        }
    }

    /**
     * @param mixed $relateds
     *
     * @return self
     */
    public function setRelateds($relateds)
    {
        $this->relateds = $relateds;

        return $this;
    }
}