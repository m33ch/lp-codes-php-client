<?php

namespace Advision\Lp\Codes\Contracts;

interface Jsonable
{
    public function toJson();
}