<?php

namespace Advision\Lp\Codes\Contracts;

interface Arrayable
{
    public function toArray();
}