<?php

namespace Advision\Lp\Codes;

use GuzzleHttp\Client as GuzzleClient;
use Advision\Lp\Codes\Exceptions\RequiredProperty;

class Authenticator extends Client
{
    protected $apikey;

    protected $secret;

    protected $userId;

    protected $token = null;

    protected $expires = null;

    public function __construct($userId, $apiKey, $secret, $host = null)
    {
        $this->apiKey = $apiKey;

        $this->userId = $userId;

        $this->secret = $secret;

        if (!$this->userId)
        {
            throw new RequiredProperty("User Id");
        }

        if (!$this->apiKey)
        {
            throw new RequiredProperty("Api key");
        }

        if (!$this->secret)
        {
            throw new RequiredProperty("Secret key");
        }

        if ($host)
        {
            self::$host = $host;
        }

        $this->setupClient();
    }

    public function getToken()
    {
        $res = $this->client->request('post', 'access/getToken', [
                'form_params' => [
                    'api_key' => $this->apiKey,
                    'api_password' => $this->secret,
                    'console_id' => $this->userId
                ]
        ]);
        $r = json_decode($res->getBody());

        if (!isset($r->token)) return null;

        $this->token = $r->token;

        $this->expires = $r->expires;

        return $this->token;
    }

    public function getExpires()
    {
        return $this->expires;
    }

    protected function setupClient()
    {
        $config = [
            'base_uri' => self::$host,
        ];

        $this->client = new GuzzleClient($config);
    }
}