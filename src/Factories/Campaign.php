<?php

namespace Advision\Lp\Codes\Factories;

use Advision\Lp\Codes\Models\Campaign as Model;

class Campaign extends SingletonFactory
{
    protected static $properties = [
        '_id'                   => null,
        'foreign_user_id'       => null,
        'foreign_campaign_id'   => null,
        'type'                  => '',
        'rules'                 => null,
        'codes'                 => null
    ];

    public static function fromObject($data)
    {
        $data = objectToArray($data);

        return self::fromArray($data);
    }

    public static function fromArray(array $data)
    {
        self::$properties = array_replace_recursive(self::$properties, $data);

        $model = new Model;

        $model->setId(self::$properties['_id'])
              ->setForeignUserId(self::$properties['foreign_user_id'])
              ->setForeignCampaignId(self::$properties['foreign_campaign_id'])
              ->setType(self::$properties['type'])
              ->setRules(self::$properties['rules'])
              ->setCodes(self::$properties['codes']);
        return $model;
    }
}