<?php

namespace Advision\Lp\Codes\Models;

class Campaign extends Model
{
    protected $foreign_user_id;

    protected $foreign_campaign_id;

    protected $type;

    protected $rules;

    protected $codes;

    protected function factoryClass()
    {
        return \Advision\Lp\Codes\Factories\Campaign::class;
    }

    public function getEndpoint()
    {
        return 'campaign';
    }

    /**
     * @return mixed
     */
    public function getForeignUserId()
    {
        return $this->foreign_user_id;
    }

    /**
     * @param mixed $foreign_user_id
     *
     * @return self
     */
    public function setForeignUserId($foreign_user_id)
    {
        $this->foreign_user_id = $foreign_user_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getForeignCampaignId()
    {
        return $this->foreign_campaign_id;
    }

    /**
     * @param string $foreign_campaign_id
     *
     * @return self
     */
    public function setForeignCampaignId($foreign_campaign_id)
    {
        $this->foreign_campaign_id = $foreign_campaign_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @param array $rules
     *
     * @return self
     */
    public function setRules($rules)
    {
        $this->rules = $rules;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodes()
    {
        return $this->codes;
    }

    /**
     * @param array $codes
     *
     * @return self
     */
    public function setCodes($codes)
    {
        $this->codes = $codes;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id'                    => $this->getId(),
            'foreign_user_id'       => $this->getForeignUserId(),
            'foreign_campaign_id'   => $this->getForeignCampaignId(),
            'type'                  => $this->getType(),
            'rules'                 => $this->getRules(),
            'codes'                 => $this->getCodes()
        ];
    }

    /**
     * @return json
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }
}

